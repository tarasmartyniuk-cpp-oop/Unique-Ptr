#pragma once
#include <type_traits>
#include <is_instantiation.h>

/*
 * this would be hidden from user in an impl namespace,
 * and all public function wrapped in T and T[] subclasses
 * 
 * instantiations pass their type as TDerived param, to enable the drill down behaviour
 * as the base needs to know the exact type of all the derived UniquePtrs to conditionally compile -> for them
 */
template <class TElem, template<typename...> class TDerived>
class UniquePtrSharedImpl
{
public:
	#pragma region Lifetime
	explicit UniquePtrSharedImpl(TElem* ptr = nullptr)
		: ptr_(ptr)
	{}

	UniquePtrSharedImpl& operator=(UniquePtrSharedImpl&& other) noexcept
	{
		if (this == &other)
		{ return *this; }

		ptr_ = other.ptr_;
		other.ptr_ = nullptr;

		return *this;
	}

	UniquePtrSharedImpl(const UniquePtrSharedImpl& other) = delete;

	virtual ~UniquePtrSharedImpl() = 0;

	UniquePtrSharedImpl& operator=(const UniquePtrSharedImpl& other) = delete;

	#pragma endregion

	TElem* Ptr() const
	{ return ptr_; }

	// is pointer non-null?
	operator bool() const
	{
		return ptr_ != nullptr;
	}

	 #pragma region Dereferencing

	// used to provide different implementations for different Pointee types
	// why can't we pass T param from class definition directly to here? 
	// (why is "Q = TPointee" partial specialization needed?)
	template<class Q = TElem>
    using CompileIfNotNestedArrowOperator = typename std::enable_if<
		! is_instantiation<TDerived, Q>::value, TElem*>::type;

	template<class Q = TElem>
    using CompileIfNestedArrowOperator = typename std::enable_if<
		is_instantiation<TDerived, Q>::value, TElem&>::type;

	/*
	 * if operator -> returns non pointer type that has overloaded the -> operator,
	 * the -> operator of that type is called.
	 * if that one returns a non-pointer that implemented -> as well, 
	 * the process repeats until the first sane(returning pointer) -> operator is encountered
	 * it is then used for name search, as if it was what the first call to -> returned
	 * 
	 * this is called the drill-down behaviour
	 * 
	 * there are, like, 10 people in the world who know and use this - the team that wrote the std smart pointers
	 * you will probably be fired outright on an attempt to use this in production code at work
	 */
	template<class Q = TElem>
	CompileIfNotNestedArrowOperator<Q> operator->() const
	{
		return Ptr();
	}

	template<class Q = TElem>
	CompileIfNestedArrowOperator<Q> operator->() const
	{
		return * Ptr();
	}

#pragma endregion

	// does not delete currently managed ptr if we attempt to reset it with itself
	// std implementation actually does that!
	void Reset(TElem* new_ptr = nullptr)
	{
		if(new_ptr != ptr_)
		{
			delete ptr_;
		}
		ptr_ = new_ptr;
	}

	TElem* Release()
	{
		auto* released = ptr_;
		ptr_ = nullptr;
		return released;
	}

protected:
	TElem* ptr_;
};

template <class T, template<typename...> class TDerived>
UniquePtrSharedImpl<T, TDerived>::~UniquePtrSharedImpl() = default;

