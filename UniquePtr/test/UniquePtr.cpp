#include <test/test.h>
#include <test/test_utils.h>
#include <UniquePtr.h>
#include <cassert>
#include <utility>
using namespace std;

struct DestructionLogger
{
	static int objects_destroyed_;
	int k = 0;

	static void Reset()
		{ objects_destroyed_ = 0; }

	~DestructionLogger()
		{ objects_destroyed_++;	}
};
int DestructionLogger::objects_destroyed_ = 0;

struct A
{
	int B;
};

struct DerivedFromA : public A {};

#pragma region Compile Time
void MoveCtor_AcceptsAllInstantiations_WithConvertiblePointers()
{
	// should compile:
	UniquePtr<DerivedFromA> derived_ptr;
	UniquePtr<A> int_ptr(move(derived_ptr));
}

void CopyCtor_CopyAssignment_Deleted()
{
	auto ptr = new int(42);
	UniquePtr<int> orig(ptr);

	// this should not compile
	//UniquePtr<int> copy(orig);
	//auto copy = orig;
}
#pragma endregion

void Destructor_DeletesPtr()
{
	LOG_START

	DestructionLogger::Reset();
	{
		UniquePtr<DestructionLogger> ptr(new DestructionLogger);
	}
	assert(DestructionLogger::objects_destroyed_ == 1);

	LogPassed();
}

void DestructorForArraySpecialization_DeletesWholeArray()
{
	LOG_START

	DestructionLogger::Reset();

	{
		UniquePtr<DestructionLogger[]> ptr(new DestructionLogger[5]);
	}
	assert(DestructionLogger::objects_destroyed_ == 5);

	LogPassed();
}

void MoveCtor_CopiesPointer()
{
	LOG_START

	auto ptr = new int(42);

	UniquePtr<int> orig(ptr);
	auto moved(move(orig));

	int l = *ptr;

	assert(moved.Ptr() == ptr);
	LogPassed();
}

void OperatorBool_True_IfPointerNonNull()
{
	LOG_START

	assert(UniquePtr<int>(new int{42}));
	LogPassed();
}

void OperatorBool_False_IfPointerNull()
{
	LOG_START

	assert(! UniquePtr<int>(nullptr));
	LogPassed();
}

void DereferenceOperator_Returns_PtrValue()
{
	LOG_START

	auto answer = new int{42};
	UniquePtr<int> ptr(answer);

	assert(* ptr == (*answer));

	LogPassed();
}

void Reset_ChangesPtrToValue()
{
	LOG_START

	UniquePtr<int> ptr(new int{42});

	auto new_ptr = new int{99};
	ptr.Reset(new_ptr);

	assert(ptr.Ptr() == new_ptr);

	LogPassed();
}

void Reset_IfNoParam_SetsToNull()
{
	LOG_START

	UniquePtr<int> ptr(new int{42});

	ptr.Reset();

	assert(ptr.Ptr() == nullptr);

	LogPassed();
}

void Release_PtrSetToNull()
{
	LOG_START

	UniquePtr<int> ptr(new int{4});

	ptr.Release();
	assert(! ptr);

	LogPassed();
}

void Release_PtrNotDeletedInDestructor()
{
	LOG_START

	int* pi = new int;
	UniquePtr<int> ptr(pi);

	ptr.Release();

	delete pi;

	LogPassed();
}

void Release_ReturnsPtr()
{
	LOG_START

	int* pi = new int;
	UniquePtr<int> ptr(pi);

	auto* released = ptr.Release();
	assert(released == pi);

	LogPassed();
}

using ANestedOneTime = UniquePtr<UniquePtr<A>>;
using ANestedTwoTimes = UniquePtr<ANestedOneTime>;

void ArrowOperator_DrillsDownToFirstNonUniquePtrTemplateParameter()
{
	LOG_START

	auto* innermost = new UniquePtr<A> (new A {42});
	auto* first_wrapper = new ANestedOneTime (innermost);
	ANestedTwoTimes second_wrapper(first_wrapper);

	// 90% of test is to check if it compiles
	assert((*first_wrapper)->B == 42);
	assert(second_wrapper->B == 42);

	LogPassed();
}

void AllTests()
{
	Destructor_DeletesPtr();
	DestructorForArraySpecialization_DeletesWholeArray();

	MoveCtor_CopiesPointer();
	MoveCtor_AcceptsAllInstantiations_WithConvertiblePointers();

	OperatorBool_True_IfPointerNonNull();
	OperatorBool_False_IfPointerNull();

	Reset_ChangesPtrToValue();
	Reset_IfNoParam_SetsToNull();

	Release_PtrSetToNull();
	Release_PtrNotDeletedInDestructor();
	Release_ReturnsPtr();

	ArrowOperator_DrillsDownToFirstNonUniquePtrTemplateParameter();

	LogAllPassed();
}