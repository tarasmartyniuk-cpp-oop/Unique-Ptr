#pragma once
#include <UniquePtrBase.h>

template <class TElem>
class UniquePtr : public UniquePtrSharedImpl<TElem, UniquePtr>
{
public:
	explicit UniquePtr(TElem* ptr = nullptr)
		: UniquePtrSharedImpl(ptr)
	{}

	/*
	 * can be constructed from any UniquePtr template instantiation,
	 * so long as this instantiation's pointer can be implicitly constructed from other's pointer
	*/
	template <class TOtherPointee>
	UniquePtr(UniquePtr<TOtherPointee>&& other)
		: UniquePtrSharedImpl(other.Release())
	{}

	~UniquePtr() override
	{ delete ptr_;	}

	TElem operator *() const
	{
		return * ptr_;
	}
};
//
template <class TElem>
class UniquePtr<TElem[]> : public UniquePtrSharedImpl<TElem, UniquePtr>
{
public:
	explicit UniquePtr(TElem* ptr = nullptr)
		: UniquePtrSharedImpl(ptr)
	{}

	template <class TOtherPointee>
	UniquePtr(UniquePtr<TOtherPointee>&& other)
		: UniquePtrSharedImpl(other.Release())
	{}

	// undefined if index >= size of allocated array, 
	// but the user knows what he's messing with by not using proper containers
	TElem& operator[] (size_t index)
	{ return *(ptr_ + index); }

	~UniquePtr() override
	{ delete[] ptr_; }
};



